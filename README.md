# Bootstrap Terraform S3 State #

Terraform config to bootstrap resources necessary to store Terraform state in
an S3 bucket, with locking via DynamoDB).

See Terraform docs for [S3 remote backend type](https://www.terraform.io/docs/backends/types/s3.html)

Thanks to:
 - [samstav's gist](https://gist.github.com/samstav/c6e1fd15ff7975abc831a8e8193eaf69)
 - https://github.com/hashicorp/terraform/issues/12877#issuecomment-289920798

# Usage #

Running `terraform plan/apply` will ask you for bucket name which is required and
project slug (name) for outputting configuration example.

At minimum something like this is required:

```sh
terraform apply --var tf_state_bucket_name=my-terraform-state
```

To get a useful example snippet to put in the project that is meant to
use the remote state, specify a project slug (URL-safe name):

```sh
$ terraform apply --var tf_state_bucket_name=my-terraform-state \
  --var project_slug=my-fancy-project
...
Outputs:

remote_state_config =
terraform {
  backend "s3" {
    bucket = "my-terraform-state"
    key = "my-fancy-project.tfstate"
    dynamodb_table = "terraform-state-my-fancy-project"
    encrypt = true
  }
}
```

If you want to store the `tfstate` file at a different path, add
`tf_state_bucket_dir` variable:

```sh
$ terraform apply --var tf_state_bucket_name=my-terraform-state \
  --var project_slug=my-fancy-project --var tf_state_bucket_dir=my-group
...
Outputs:

remote_state_config =
terraform {
  backend "s3" {
    bucket = "my-terraform-state"
    key = "my-group/my-fancy-project.tfstate"
    dynamodb_table = "terraform-state-my-fancy-project"
    encrypt = true
  }
}
```

Note that terraform will store the name of the table in state file, so
if you try to apply this again with a different table name next time it
would fail as it would try to destroy the previously created table.  In
order to deal with that you have to make terraform forget about the
previous table, which you can do with state manipulation:

```sh
# show previously created table, if this doesn't output anything you
# can skip remaining commands
$ terraform state show aws_dynamodb_table.tf_state_table
id                        = terraform-state-my-project-1
arn                       = arn:aws:dynamodb:eu-west-1:566549752315:table/terraform-state-my-project-1
attribute.#               = 1

# move previously created table to another state file (as a backup)
$ terraform state mv --state-out=terraform-my-project-1.tfstate aws_dynamodb_table.tf_state_table aws_dynamodb_table.tf_state_table
Moved aws_dynamodb_table.tf_state_table to aws_dynamodb_table.tf_state_table
```

Now apply with another project name will just create a new table
without touching the old one.

If you ever want to restore and operate on a previously created table,
you can do:

```sh
# make sure there is no table in current state
$ terraform state show aws_dynamodb_table.tf_state_table

# restore previous table into current state
$ terraform state mv --state=terraform-prod-code-check.tfstate --state-out=terraform.tfstate aws_dynamodb_table.tf_state_table aws_dynamodb_table.tf_state_table
Moved aws_dynamodb_table.tf_state_table to aws_dynamodb_table.tf_state_table
```

# Variables #

The following variables can be customised (using `--var` option to
`terraform plan/apply`):

<dl>
  <dt>tf_state_bucket_name (required)</dt>
  <dd>
    Name of the S3 bucket to create for storing the state.
    Single bucket is enough for multiple projects.
  </dd>
  <dt>project_slug</dt>
  <dd>
    Project slug (URL-safe name). This is only used in generated
    example snippets for use in actual projects.
  </dd>
  <dt>tf_state_table_name</dt>
  <dd>
    Name of the DynamoDB table to use for locking (will be prefixed
    with `terraform-state-`) (default: ${project_slug}).  There should
    be one table per project if orchestrating multiple projects
    concurrently is desired. If not required a table name like "common"
    can be used for multiple projects.
  </dd>
  <dt>tf_state_bucket_dir</dt>
  <dd>
    Path to the store .tfstate files at in the bucket. This is optional
    only used in the generated example snippets.
  </dd>
</dl>
