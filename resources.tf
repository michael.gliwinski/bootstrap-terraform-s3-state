# bootstrap-terraform-s3-state
# Copyright (C) 2017  HendosDO
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

variable "tf_state_bucket_name" {
  description = "Name of the S3 bucket to create for storing the state"
}

variable "project_slug" {
  description = "Project slug (URL-safe name), for examples"
  default = "MY-PROJECT-REPLACE-ME"
}

variable "tf_state_table_name" {
  default = ""
  description = <<-EOF
    Name of the DynamoDB table to use for locking (will be prefixed with
    terraform-state-) (default: project_slug)
    EOF
}

variable "tf_state_bucket_dir" {
  default = ""
  description = "Path to the store .tfstate files at in the bucket, optional, for examples"
}

locals {
  default_state_table_name = "${var.project_slug}"

  tf_state_bucket_key = "${replace(join("/", list(var.tf_state_bucket_dir, var.project_slug)), "/^\\//", "")}.tfstate"
  tf_state_table_name = "terraform-state-${var.tf_state_table_name != "" ? var.tf_state_table_name : local.default_state_table_name}"
}

resource "aws_s3_bucket" "tf_state_bucket" {
  bucket = "${var.tf_state_bucket_name}"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "tf_state_table" {
  name = "${local.tf_state_table_name}"
  read_capacity = 1
  write_capacity = 1
  hash_key = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  lifecycle {
    prevent_destroy = true
  }
}

output "remote_state_config" {
  value = <<EOF

terraform {
  backend "s3" {
    bucket = "${aws_s3_bucket.tf_state_bucket.bucket}"
    key = "${local.tf_state_bucket_key}"
    dynamodb_table = "${local.tf_state_table_name}"
    encrypt = true
  }
}
EOF
}
